
IfByPhone Click-to-call Drupal Module

(c)2009 - Licensed under the GPLv2 - See LICENSE.txt for details.

To install...

1. Copy the clicktocall module files to your sites/all/modules directory.
Enable the module, in your system menu at Administer > Site building > Modules.

2. Sign up for an account at the IfByPhone web site...
      http://ifbyphone.com

3. Visit your clicktocall settings page, under the system menu at Administer >
Site configuration > Click-to-call. You'll need to enter your IfByPhone API
public key, and your Click ID. You can obtain these by logging into your
IfByPhone account.

4. (optional) Visit your blocks configuration page, under the system menu at
Administer > Site building > Blocks. Assign the IfByPhone Click-to-call block
to a region in your page template.

5. (optional) Should you wish to insert the Click-to-call form in content other
than a block, enable the PHP Filter module and use this PHP code...

<?php
print clicktocall_display();
?>

