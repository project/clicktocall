<?php

/*
 IfByPhone - http://www.ifbyphone.com
 Copyright (c) 2009 Ifbyphone, Inc.
 ClickToCall Module Helper

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 documentation files (the "Software"), to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
 to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions
 of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.

 This file contains helper functions that will take the AJAX call and redirect it through to the correct page on the IBP domain

*/

// anti-spoof value must be stored in cookie
if (!isset($_COOKIE['clicktocall_id']) || (int)$_COOKIE['clicktocall_id'] < (time()-(60*60*24))) {
	echo '<b>Your ClickToCall session has expired, please refresh the page you are on and try again.</b>';
	exit;
}

$url = "http://www.ifbyphone.com/click_to_xyz.php?";

// get clean variables to pass to IBP server
$click_id      = clean($_GET['click_id']);
$ref           = clean($_GET['ref']);
$page          = clean($_GET['page']);
$phone_to_call = clean($_GET['phone_to_call']);
$key           = clean($_GET['key']);
$ibp_referrer  = clean($_GET['ibp_referrer']);

$url .= "click_id=".rawurlencode($click_id);
$url .= "&ref=".rawurlencode($ref);
$url .= "&page=".rawurlencode($page);
$url .= "&phone_to_call=".rawurlencode($phone_to_call);
$url .= "&key=".rawurlencode($key);
$url .= "&ibp_referrer=".rawurlencode($ibp_referrer);

// Open the CURL session
$session = curl_init($url);

// Don't return HTTP headers. Do return the contents of the call
curl_setopt($session, CURLOPT_HEADER, false);
curl_setopt($session, CURLOPT_RETURNTRANSFER, true);

// Make the call
$text = curl_exec($session);

// The web service returns XML
header("Content-Type: text/plain");

echo $text;

curl_close($session);


function clean($source) {
	// clean out any unwanted whitespace characters
	$source = preg_replace('/[\t\n\r]/','',$source);
	
	// clean out any low control chars
	$source = preg_replace('/[\x00-\x09\x11-\x1F\x7F]/', '', $source);

	// clean out any high control chars
	$source = preg_replace('/[\xA0-\xFF]/', '', $source);
			
	return (string)$source;
}

?>